TEMPLATE = subdirs
SUBDIRS += \
    applet \
    test \
    set-ipv4-method \
    list-services \
    test-clock \
    openconnect \
    openconnect-vpnd \
    list-vpn-connections \
    test-vpnmanager
