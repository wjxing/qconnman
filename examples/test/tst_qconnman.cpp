/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "manager.h"

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtWidgets/QtWidgets>
#else
#include <QtGui/QtGui>
#endif

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    Manager model;
    QWidget window;
    QHBoxLayout layout(&window);

    QTreeView view(&window);
    view.setModel(&model);
    layout.addWidget(&view);

    QListView technologyView(&window);
    technologyView.setModel(&model);
    layout.addWidget(&technologyView);

    QListView serviceView(&window);
    serviceView.setModel(&model);
    layout.addWidget(&serviceView);

    QObject::connect(&technologyView, SIGNAL(clicked(QModelIndex)), &serviceView, SLOT(setRootIndex(QModelIndex)));
    QObject::connect(&model, SIGNAL(rowsInserted(QModelIndex,int,int)), &view, SLOT(expandAll()));
    QObject::connect(&model, SIGNAL(rowsRemoved(QModelIndex,int,int)), &view, SLOT(expandAll()));

    window.show();
    return app.exec();
}
