/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QMenu>
#include <QSystemTrayIcon>
#include <QDebug>
#include <QAction>
#include <QPushButton>
#include <QWidgetAction>
#include <QTreeView>
#include <QHeaderView>
#include <QDBusConnection>

#include "agent.h"
#include "manager.h"
#include "managermenuaction.h"
#include "technologywidget.h"
#include "servicewidget.h"
#include "trayiconcontroller.h"

TrayIconController::TrayIconController(QObject *parent)
    : QObject(parent),
      m_manager(0),
      m_agent(0),
      m_systemTrayIcon(0),
      m_networkView(0)
{
    m_manager = new Manager(this);
    connect(m_manager, SIGNAL(stateChanged()), this, SLOT(managerStateChanged()));
    connect(m_manager, SIGNAL(offlineModeChanged()), this, SLOT(managerOfflineModeChanged()));

    m_systemTrayIcon =
        new QSystemTrayIcon(QIcon(":/images/network-offline.png"));
    connect(m_systemTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
                        this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

    // setup agent
    m_agent = new Agent("/DevonIT/Agent", m_manager);
    m_manager->registerAgent(m_agent);

    // create network view
    m_networkView = new QTreeView;
    m_networkView->header()->hide();
    m_networkView->setSelectionMode(QAbstractItemView::NoSelection);
    m_networkView->setFixedWidth(300);
    m_networkView->resize(300, 500);
    connect(m_manager, SIGNAL(rowsInserted(QModelIndex,int,int)),
                 this, SLOT(createIndexWidgets(QModelIndex,int,int)));
    connect(m_manager, SIGNAL(rowsInserted(QModelIndex,int,int)), m_networkView, SLOT(expandAll()));
    connect(m_manager, SIGNAL(rowsRemoved(QModelIndex,int,int)), m_networkView, SLOT(expandAll()));

    m_networkView->setModel(m_manager);
    m_networkView->show();

    /*
    QWidgetAction *networkViewAction = new QWidgetAction(this);
    networkViewAction->setDefaultWidget(m_networkView);

    m_managerMenu = new QMenu;
    m_managerMenu->addAction(networkViewAction);
    m_systemTrayIcon->setContextMenu(m_managerMenu);
    */

    initializeIndexWidgets();
    m_systemTrayIcon->show();
}

TrayIconController::~TrayIconController()
{
    m_manager->deleteLater();
    m_systemTrayIcon->deleteLater();
    m_networkView->deleteLater();
}

void TrayIconController::initializeIndexWidgets()
{
    createIndexWidgets(QModelIndex(), 0, m_manager->rowCount());
    for (int row = 0; row < m_manager->rowCount(); ++row) {
        QModelIndex parent = m_manager->index(row, 0);
        createIndexWidgets(parent, 0, m_manager->rowCount(parent));
    }
}

void TrayIconController::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    qDebug() << "activation reason: " << reason;
}

void TrayIconController::createIndexWidgets(const QModelIndex &parent, int start, int end)
{
    for (int row = start; row < end; row++) {
        QModelIndex idx = m_manager->index(row, 0, parent);
        ManagerNode *node = static_cast<ManagerNode*>(idx.internalPointer());
        if (node->isTechnology()) {
            m_networkView->setIndexWidget(idx, new TechnologyWidget(node->object<Technology*>()));
        } else if (node->isService())
            m_networkView->setIndexWidget(idx, new ServiceWidget(node->object<Service*>()));
    }
}

void TrayIconController::managerStateChanged()
{
    if (m_manager->state() == Manager::Online) {
        m_systemTrayIcon->setIcon(QIcon(":/images/online.png"));
    } else {
        m_systemTrayIcon->setIcon(QIcon(":/images/offline.png"));
    }
}

void TrayIconController::managerOfflineModeChanged()
{
    if (m_manager->offlineMode()) {
        m_systemTrayIcon->setIcon(QIcon(":/images/offline-mode.png"));
    } else {
        managerStateChanged();
    }
}

