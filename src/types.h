/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef TYPES_H
#define TYPES_H

#include <QDBusArgument>
#include <QDBusObjectPath>

struct ObjectPropertyData
{
    QDBusObjectPath path;
    QVariantMap properties;
};
Q_DECLARE_METATYPE(ObjectPropertyData)
Q_DECLARE_METATYPE(QList<ObjectPropertyData>)
QDBusArgument &operator<<(QDBusArgument &argument, const ObjectPropertyData &data);
const QDBusArgument &operator>>(const QDBusArgument &argument, ObjectPropertyData &data);

typedef QMap<QString, QString> QStringMap;
//Q_DECLARE_METATYPE(QStringMap)

#endif
