GENERATED_INTERFACES_HEADERS += \
    $${PWD}/clock_interface.h \
    $${PWD}/manager_interface.h \
    $${PWD}/service_interface.h \
    $${PWD}/technology_interface.h \
    $${PWD}/vpn_manager_interface.h \
    $${PWD}/vpn_connection_interface.h

GENERATED_INTERFACES_SOURCES += \
    $${PWD}/clock_interface.cpp \
    $${PWD}/manager_interface.cpp \
    $${PWD}/service_interface.cpp \
    $${PWD}/technology_interface.cpp \
    $${PWD}/vpn_manager_interface.cpp \
    $${PWD}/vpn_connection_interface.cpp
