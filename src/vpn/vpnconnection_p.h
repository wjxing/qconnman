#ifndef VPNCONNECTION_P_H
#define VPNCONNECTION_P_H

#include <QStringList>
#include <QDBusObjectPath>
#include <QString>

class VpnRouteDataPrivate
{
public:
    int protocolFamily;
    QString network;
    QString netmask;
    QString gateway;
};

class IPV4Data;
class IPV6Data;
class VpnRouteData;
class VpnConnection;
class NetConnmanVpnConnectionInterface;
class VpnConnectionPrivate
{
public:
    VpnConnectionPrivate(const QDBusObjectPath &path, VpnConnection *qq);
    void initialize(const QVariantMap &properties);

    // private slots
    void propertyChanged(const QString &property, const QDBusVariant &value);

    static int s_vpnRouteDataMetaTypeId;
    static int s_vpnRouteDataListMetaTypeId;
    static int s_ipv4DataMetaTypeId;
    static int s_ipv6DataMetaTypeId;

    NetConnmanVpnConnectionInterface *connectionInterface;

    QDBusObjectPath objectPath;
    QString state;
    QString type;
    QString domain;
    QString host;
    bool immutable;
    int index;
    IPV4Data *ipv4Data;
    IPV6Data *ipv6Data;
    QStringList nameservers;
    QList<VpnRouteData*> userRoutes;
    QList<VpnRouteData*> serverRoutes;

    VpnConnection * const q_ptr;
    Q_DECLARE_PUBLIC(VpnConnection)
};

class OpenConnectDataPrivate
{
public:
    OpenConnectDataPrivate()
        : noCertCheck(false)
    {
    }

    QString cookie;
    QString serverCert;
    QString caCert;
    QString clientCert;
    QString vpnHost;
    bool noCertCheck;

};

class OpenConnectData;
class OpenConnectVpnConnection;
class OpenConnectVpnConnectionPrivate : public VpnConnectionPrivate
{
public:
    OpenConnectVpnConnectionPrivate(const QDBusObjectPath &path, VpnConnection *qq);

    OpenConnectData *data;
    static int s_openConnectDataMetaTypeId;

};

#endif
