#include <QDBusServiceWatcher>
#include <QDebug>

#include "interfaces/vpn_manager_interface.h"

#include "qconnman_debug.h"
#include "vpnconnection.h"
#include "vpnmanager_p.h"
#include "vpnmanager.h"

VpnManager::VpnManager(QObject *parent)
    : QAbstractListModel(parent),
      d_ptr(new VpnManagerPrivate(this))
{
    Q_D(VpnManager);
    d->connmanVpnWatcher = new QDBusServiceWatcher("net.connman.vpn.Manager",
       QDBusConnection::systemBus(),
       QDBusServiceWatcher::WatchForRegistration | QDBusServiceWatcher::WatchForUnregistration,
       this);
    connect(d->connmanVpnWatcher, SIGNAL(serviceRegistered(QString)), this, SLOT(connmanVpnRegistered()));
    connect(d->connmanVpnWatcher, SIGNAL(serviceUnregistered(QString)), this, SLOT(connmanVpnUnregistered()));

    // we could be starting after connman, so fake the first one
    d->connmanVpnRegistered();
}

VpnManager::~VpnManager()
{
}

void VpnManagerPrivate::connmanVpnRegistered()
{
    Q_Q(VpnManager);
    qConnmanDebug() << Q_FUNC_INFO;
    if (managerInterface)
        managerInterface->deleteLater();

    managerInterface =
        new NetConnmanVpnManagerInterface("net.connman.vpn", "/", QDBusConnection::systemBus(), q);

    if (!managerInterface->isValid()) {
        qConnmanDebug() << "vpn manager interface is invalid, aborting...";
        return;
    }

    QObject::connect(managerInterface, SIGNAL(ConnectionAdded(QDBusObjectPath,QVariantMap)),
                                    q, SLOT(connectionAdded(QDBusObjectPath,QVariantMap)));
    QObject::connect(managerInterface, SIGNAL(ConnectionRemoved(QDBusObjectPath)),
                                    q, SLOT(connectionRemoved(QDBusObjectPath)));

    // get the initial list of connections
    // technologies
    QDBusPendingReply<QList<ObjectPropertyData> > reply = managerInterface->GetConnections();
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, q);
    QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                           q, SLOT(getConnectionsResponse(QDBusPendingCallWatcher*)));

    // for command line interface
    watcher->waitForFinished();
}

void VpnManagerPrivate::getConnectionsResponse(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QList<ObjectPropertyData> > reply = *call;
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error().message();
    } else {
        QList<ObjectPropertyData> result = reply.value();
        foreach (ObjectPropertyData data, result)
            connectionAdded(data.path, data.properties);
    }

    call->deleteLater();
}

void VpnManagerPrivate::connmanVpnUnregistered()
{
    Q_Q(VpnManager);
    qConnmanDebug() << Q_FUNC_INFO;
    if (managerInterface) {
        managerInterface->deleteLater();
        managerInterface = 0;
    }

    q->beginRemoveRows(QModelIndex(), 0, connections.size());
    while (VpnConnection *connection = connections.takeLast())
        connection->deleteLater();
    q->endRemoveRows();
}

int VpnManagerPrivate::indexOfConnection(const QDBusObjectPath &path) const
{
    for (int i = 0; i < connections.size(); ++i) {
        if (connections.value(i)->objectPath() == path)
            return i;
    }

    return -1;
}

void VpnManagerPrivate::connectionAdded(const QDBusObjectPath &path, const QVariantMap &properties)
{
    Q_Q(VpnManager);
    int index = indexOfConnection(path);
    if (index != -1) {
        qConnmanDebug() << "attempted addition of existing connection(" << path.path() << ")";
        return;
    }

    VpnConnection *connection;
    QString type = properties.value("Type").toString();
    if (type  == QLatin1String("openconnect"))
        connection = new OpenConnectVpnConnection(path, properties, q);
    else
        connection = new VpnConnection(path, properties, q);
    q->beginInsertRows(QModelIndex(), connections.size(), connections.size() + 1);
    connections.append(connection);
    q->endInsertRows();

    qConnmanDebug() << "added vpn connection(" << path.path() << ")";
}

void VpnManagerPrivate::connectionRemoved(const QDBusObjectPath &path)
{
    Q_Q(VpnManager);
    int index = indexOfConnection(path);
    if (index == -1) {
        qConnmanDebug() << "attempted removal of unknown vpn connection(" << path.path() << ")";
        return;
    }

    q->beginRemoveRows(QModelIndex(), index, index);
    VpnConnection *connection = connections.takeAt(index);
    connection->deleteLater();
    q->endRemoveRows();

    qConnmanDebug() << "removed vpn connection(" << path.path() << ")";
}

QDBusObjectPath VpnManager::create(const QVariantMap &providerData)
{
    Q_D(VpnManager);
    QDBusPendingReply<QDBusObjectPath> result = d->managerInterface->Create(providerData);
    result.waitForFinished();
    if (!result.isValid() || result.isError()) {
        qConnmanDebug() << "failed to create vpn connection: " << result.error();
        return QDBusObjectPath();
    }

    return result.value();
}

void VpnManager::remove(const QDBusObjectPath &path)
{
    Q_D(VpnManager);
    QDBusPendingReply<> result = d->managerInterface->Remove(path);
    result.waitForFinished();
    if (!result.isValid() || result.isError()) {
        qConnmanDebug() << "failed to remove vpn connection: " << result.error();
    }

    qConnmanDebug() << "removed vpn connection(" << path.path() << ")";
}

void VpnManager::remove(VpnConnection *connection)
{
    remove(connection->objectPath());
}

QList<VpnConnection*> VpnManager::connections() const
{
    Q_D(const VpnManager);
    return d->connections;
}

VpnConnection *VpnManager::connection(const QDBusObjectPath &path) const
{
    Q_D(const VpnManager);
    int index = d->indexOfConnection(path);
    if (index == -1)
        return 0;
    return d->connections.at(index);
}

void VpnManager::registerAgent(VpnAgent *agent)
{
    Q_UNUSED(agent)
    qDebug() << Q_FUNC_INFO << "UNIMPLEMENTED";
}

void VpnManager::unregisterAgent(VpnAgent *agent)
{
    Q_UNUSED(agent)
    qDebug() << Q_FUNC_INFO << "UNIMPLEMENTED";
}

void VpnManager::unregisterAgent(const QDBusObjectPath &path)
{
    Q_UNUSED(path)
    qDebug() << Q_FUNC_INFO << "UNIMPLEMENTED";
}

QVariant VpnManager::data(const QModelIndex &index, int role) const
{
    Q_D(const VpnManager);
    if (index.row() < 0 || index.row() > d->connections.size())
        return QVariant();

    if (role == Qt::DisplayRole)
        return d->connections.at(index.row())->objectPath().path();
    return QVariant();
}

int VpnManager::rowCount(const QModelIndex &parent) const
{
    Q_D(const VpnManager);
    Q_UNUSED(parent)
    return d->connections.size();
}

#include "moc_vpnmanager.cpp"
