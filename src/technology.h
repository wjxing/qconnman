/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef TECHNOLOGY_H
#define TECHNOLOGY_H

#include <QObject>
#include <QString>
#include <QDBusVariant>
#include <QScopedPointer>

#include "connmanobject.h"

class TechnologyPrivate;
class Technology : public ConnManObject
{
    Q_OBJECT
    Q_PROPERTY(bool Powered READ isPowered WRITE setPoweredInternal NOTIFY poweredChanged)
    Q_PROPERTY(bool Connected READ isConnected WRITE setConnectedInternal NOTIFY connectedChanged)
    Q_PROPERTY(QString Name READ name WRITE setNameInternal NOTIFY nameChanged)
    Q_PROPERTY(QString Type READ type WRITE setTypeInternal NOTIFY typeChanged)
    Q_PROPERTY(bool Tethering READ tetheringAllowed WRITE setTetheringAllowedInternal NOTIFY tetheringAllowedChanged)
    Q_PROPERTY(QString TetheringIdentifier READ tetheringIdentifier WRITE setTetheringIdentifierInternal NOTIFY tetheringIdentifierChanged)
    Q_PROPERTY(QString TetheringPassphrase READ tetheringPassphrase WRITE setTetheringPassphraseInternal NOTIFY tetheringPassphraseChanged)
public:
    explicit Technology(const QDBusObjectPath &path, const QVariantMap &properties, QObject *parent = 0);
    ~Technology();

    QDBusObjectPath path() const;
    QString name() const;
    QString type() const;
    bool isConnected() const;

    bool isPowered() const;
    void setPowered(bool powered);

    bool tetheringAllowed() const;
    void setTetheringAllowed(bool allowed);

    QString tetheringIdentifier() const;
    void setTetheringIdentifier(const QString &identifier);

    QString tetheringPassphrase() const;
    void setTetheringPassphrase(const QString &passphrase);

Q_SIGNALS:
    void poweredChanged(bool powered);
    void connectedChanged();
    void nameChanged();
    void typeChanged();
    void tetheringAllowedChanged();
    void tetheringIdentifierChanged();
    void tetheringPassphraseChanged();

    void scanCompleted();

public Q_SLOTS:
    void scan();

private:
    void setPoweredInternal(bool powered);
    void setConnectedInternal(bool connected);
    void setNameInternal(const QString &name);
    void setTypeInternal(const QString &type);
    void setTetheringAllowedInternal(bool allowed);
    void setTetheringIdentifierInternal(const QString &identifier);
    void setTetheringPassphraseInternal(const QString &passphrase);

    Q_DISABLE_COPY(Technology)
    Q_DECLARE_PRIVATE(Technology)
    QScopedPointer<TechnologyPrivate> d_ptr;

};
Q_DECLARE_METATYPE(Technology*)

#endif

