include(../qconnman.pri)
include(interfaces/interfaces.pri)
include(adaptors/adaptors.pri)
include(vpn/vpn.pri)

TEMPLATE = lib
TARGET = qconnman
DESTDIR = ../lib
DEPENDPATH += .
INCLUDEPATH += .
CONFIG += $${QCONNMAN_LIBRARY_TYPE}
VERSION = $${QCONNMAN_VERSION}
QT += dbus

# Input
PRIVATE_HEADERS = \
    service_p.h \
    clock_p.h \
    manager_p.h \
    technology_p.h \
    serviceconfiguration_p.h \
    $${VPN_PRIVATE_HEADERS} \
    $${GENERATED_ADAPTORS_HEADERS} \
    $${GENERATED_INTERFACES_HEADERS}

PUBLIC_HEADERS += \
    connmanobject.h \
    manager.h \
    service.h \
    technology.h \
    agent.h \
    types.h \
    clock.h \
    configurationformat.h \
    serviceconfiguration.h \
    qconnman_debug.h

HEADERS += \
    $${PUBLIC_HEADERS} \
    $${VPN_HEADERS} \
    $${PRIVATE_HEADERS}

SOURCES += \
    connmanobject.cpp \
    manager.cpp \
    service.cpp \
    technology.cpp \
    agent.cpp \
    types.cpp \
    clock.cpp \
    configurationformat.cpp \
    serviceconfiguration.cpp \
    $${VPN_SOURCES} \
    $${GENERATED_ADAPTORS_SOURCES} \
    $${GENERATED_INTERFACES_SOURCES}

target.path = $${PREFIX}/$${LIBDIR}
header_files.files = $${PUBLIC_HEADERS}
header_files.path = $${PREFIX}/include/qconnman
INSTALLS += target header_files

# pkg-config support
CONFIG += create_pc create_prl no_install_prl

QMAKE_PKGCONFIG_NAME = qconnman
QMAKE_PKGCONFIG_DESCRIPTION = QConnman provides a Qt wrapper around connman, the open source connection manager
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$header_files.path
equals(QCONNMAN_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQCONNMAN_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQCONNMAN_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl

OTHER_FILES += \
    dbus/net.connman.Agent.xml \
    dbus/net.connman.Clock.xml \
    dbus/net.connman.Manager.xml \
    dbus/net.connman.Service.xml \
    dbus/net.connman.Technology.xml \
    dbus/net.connman.vpn.Manager.xml \
    dbus/net.connman.vpn.Connection.xml \
    dbus/net.connman.vpn.Agent.xml
