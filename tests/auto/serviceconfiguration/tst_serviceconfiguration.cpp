#include <QtTest/QtTest>
#include <QStringList>
#include <QDebug>

#include "manager.h"
#include "serviceconfiguration.h"

class tst_ServiceConfiguration : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void init();

    void writeHome_ethernet();
    void writeHome_ethernetConfiguration();
    void readHome_ethernet();
    void writeHome_wifi();
    void readHome_wifi();
    void loadFromService();

    void configuredServices_data();
    void configuredServices();

private:
    bool compareProvisionFiles(const QByteArray &expected, const QByteArray &actual);
    QScopedPointer<QTemporaryFile> actual;

};

bool tst_ServiceConfiguration::compareProvisionFiles(const QByteArray &expected, const QByteArray &actual)
{
    QBuffer buffer;
    buffer.setData(expected);
    if (!buffer.open(QIODevice::ReadOnly))
        return false;

    while (buffer.canReadLine()) {
        QByteArray line = buffer.readLine();
        if (!actual.contains(line))
            return false;
    }

    return true;
}

void tst_ServiceConfiguration::init()
{
    actual.reset(new QTemporaryFile);
    actual->setAutoRemove(true);
    QVERIFY(actual->open());
}

void tst_ServiceConfiguration::writeHome_ethernet()
{
    ServiceConfiguration service("home_ethernet", actual->fileName());
    service.ipv4()->setAddress("192.168.1.42");
    service.ipv4()->setNetmask("255.255.255.0");
    service.ipv4()->setGateway("192.168.1.1");
    service.ipv4()->apply();

    service.ipv6()->setAddress("2001:db8::42");
    service.ipv6()->setPrefixLength("64");
    service.ipv6()->setGateway("2001:db8::1");
    service.ipv6()->apply();

    service.setMacAddress("01:02:03:04:05:06");

    service.setNameserversConfiguration(QStringList() << "10.2.3.4" << "192.168.1.99");
    service.setDomainsConfiguration(QStringList() << "my.home" << "isp.net");
    service.setTimeserversConfiguration(QStringList() << "10.172.2.1" << "ntp.my.isp.net");
    service.setDomain("my.home");
    QVERIFY(service.save());

    QFile expected(":/configs/home_ethernet.config");
    QVERIFY(expected.open(QIODevice::ReadOnly));
    QVERIFY(compareProvisionFiles(expected.readAll(), actual->readAll()));
}

void tst_ServiceConfiguration::writeHome_ethernetConfiguration()
{
    ServiceConfiguration service("home_ethernet", actual->fileName());
    service.ipv4Configuration()->setAddress("192.168.1.42");
    service.ipv4Configuration()->setNetmask("255.255.255.0");
    service.ipv4Configuration()->setGateway("192.168.1.1");
    service.ipv4Configuration()->apply();

    service.ipv6Configuration()->setAddress("2001:db8::42");
    service.ipv6Configuration()->setPrefixLength("64");
    service.ipv6Configuration()->setGateway("2001:db8::1");
    service.ipv6Configuration()->apply();

    service.setMacAddress("01:02:03:04:05:06");

    service.setNameserversConfiguration(QStringList() << "10.2.3.4" << "192.168.1.99");
    service.setDomainsConfiguration(QStringList() << "my.home" << "isp.net");
    service.setTimeserversConfiguration(QStringList() << "10.172.2.1" << "ntp.my.isp.net");
    service.setDomain("my.home");
    QVERIFY(service.save());

    QFile expected(":/configs/home_ethernet.config");
    QVERIFY(expected.open(QIODevice::ReadOnly));
    QVERIFY(compareProvisionFiles(expected.readAll(), actual->readAll()));
}

void tst_ServiceConfiguration::readHome_ethernet()
{
    QFile config(":/configs/home_ethernet.config");
    QVERIFY(config.open(QIODevice::ReadOnly));
    actual->write(config.readAll());
    actual->seek(0);

    ServiceConfiguration service("home_ethernet", actual->fileName());
    QCOMPARE(service.ipv4()->address(), QLatin1String("192.168.1.42"));
    QCOMPARE(service.ipv4()->netmask(), QLatin1String("255.255.255.0"));
    QCOMPARE(service.ipv4()->gateway(), QLatin1String("192.168.1.1"));

    QCOMPARE(service.ipv6()->address(), QLatin1String("2001:db8::42"));
    QCOMPARE(service.ipv6()->prefixLength(), QLatin1String("64"));
    QCOMPARE(service.ipv6()->gateway(), QLatin1String("2001:db8::1"));

    QCOMPARE(service.macAddress(), QLatin1String("01:02:03:04:05:06"));

    QCOMPARE(service.nameservers(), QStringList() << "10.2.3.4" << "192.168.1.99");
    QCOMPARE(service.domains(), QStringList() << "my.home" << "isp.net");
    QCOMPARE(service.timeservers(), QStringList() << "10.172.2.1" << "ntp.my.isp.net");
    QCOMPARE(service.domain(), QLatin1String("my.home"));
}

void tst_ServiceConfiguration::writeHome_wifi()
{
    WifiServiceConfiguration service("home_wifi", actual->fileName());
    service.ipv4()->setAddress("192.168.2.2");
    service.ipv4()->setNetmask("255.255.255.0");
    service.ipv4()->setGateway("192.168.2.1");
    service.ipv4()->apply();

    service.setMacAddress("06:05:04:03:02:01");
    service.setPassphrase("secret");
    service.setName("my_home_wifi");
    QVERIFY(service.save());

    QFile expected(":/configs/home_wifi.config");
    QVERIFY(expected.open(QIODevice::ReadOnly));
    QVERIFY(compareProvisionFiles(expected.readAll(), actual->readAll()));
}

void tst_ServiceConfiguration::readHome_wifi()
{
    QFile config(":/configs/home_wifi.config");
    QVERIFY(config.open(QIODevice::ReadOnly));
    actual->write(config.readAll());
    actual->seek(0);

    WifiServiceConfiguration service("home_wifi", actual->fileName());
    QCOMPARE(service.ipv4()->address(), QLatin1String("192.168.2.2"));
    QCOMPARE(service.ipv4()->netmask(), QLatin1String("255.255.255.0"));
    QCOMPARE(service.ipv4()->gateway(), QLatin1String("192.168.2.1"));
    QCOMPARE(service.macAddress(), QLatin1String("06:05:04:03:02:01"));
    QCOMPARE(service.passphrase(), QLatin1String("secret"));
    QCOMPARE(service.name(), QLatin1String("my_home_wifi"));
}

void tst_ServiceConfiguration::loadFromService()
{
    QFile config(":/configs/home_wifi.config");
    QVERIFY(config.open(QIODevice::ReadOnly));
    actual->write(config.readAll());
    actual->seek(0);

    WifiServiceConfiguration service("home_wifi", actual->fileName());
    QCOMPARE(service.ipv4()->address(), QLatin1String("192.168.2.2"));
    QCOMPARE(service.ipv4()->netmask(), QLatin1String("255.255.255.0"));
    QCOMPARE(service.ipv4()->gateway(), QLatin1String("192.168.2.1"));
    QCOMPARE(service.macAddress(), QLatin1String("06:05:04:03:02:01"));
    QCOMPARE(service.passphrase(), QLatin1String("secret"));
    QCOMPARE(service.name(), QLatin1String("my_home_wifi"));

    QTemporaryFile tempFile;
    tempFile.setAutoRemove(true);
    QVERIFY(tempFile.open());
    WifiServiceConfiguration newService(&service, actual->fileName());
    QCOMPARE(newService.ipv4()->address(), QLatin1String("192.168.2.2"));
    QCOMPARE(newService.ipv4()->netmask(), QLatin1String("255.255.255.0"));
    QCOMPARE(newService.ipv4()->gateway(), QLatin1String("192.168.2.1"));
    QCOMPARE(newService.macAddress(), QLatin1String("06:05:04:03:02:01"));
    QCOMPARE(newService.passphrase(), QLatin1String("secret"));
    QCOMPARE(newService.name(), QLatin1String("my_home_wifi"));
    QVERIFY(newService.save());

    tempFile.seek(0);
    QByteArray data = tempFile.readAll();
    QVERIFY(data.isEmpty());
}

void tst_ServiceConfiguration::configuredServices_data()
{
    QTest::addColumn<QString>("configurationFile");
    QTest::addColumn<QStringList>("expected");

    QTest::newRow("home_ethernet") << ":/configs/home_ethernet.config" << (QStringList() << "home_ethernet");
    QTest::newRow("home_wifi") << ":/configs/home_wifi.config" << (QStringList() << "home_wifi");
    QTest::newRow("peap") << ":/configs/peap.config" << (QStringList() << "peap");
    QTest::newRow("tls") << ":/configs/tls.config" << (QStringList() << "tls");
    QTest::newRow("example") << ":/configs/example.config"
                             << (QStringList() << "home_ethernet" << "home_wifi" << "peap" << "ttls");
}

void tst_ServiceConfiguration::configuredServices()
{
    QFETCH(QString, configurationFile);
    QFETCH(QStringList, expected);

    QStringList actual = Manager::configuredServices(configurationFile);
    QCOMPARE(actual, expected);
}

QTEST_MAIN(tst_ServiceConfiguration)
#include "tst_serviceconfiguration.moc"
