DEPTH = ../../..
include($${DEPTH}/qconnman.pri)
include($${DEPTH}/tests/tests.pri)

TEMPLATE = app
TARGET = tst_serviceconfiguration
RESOURCES += \
    configs.qrc

SOURCES += \
    tst_serviceconfiguration.cpp
